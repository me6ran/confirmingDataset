import os
import matplotlib.pyplot as plt
import cv2
import xml.etree.ElementTree as ET
from shutil import copyfile


AnnFolder="Annotations"
imageFolder="Images"
collected="./Collected"
imageExt=['jpg','png',"JPG","PNG"]
lastFile="lastFile"
fileAnchor=0

def checkExtension(fileExt):
    return fileExt in imageExt

if not ((os.path.isdir(AnnFolder)) and (os.path.isdir(imageFolder))):
    print("there is no %s or %s folder(s)" % (AnnFolder,imageFolder))
    exit(1)

if not (os.path.isdir(collected)):
    os.mkdir(collected)

if not (os.path.isfile(lastFile)):
    fileNumber=0
    fd=open(lastFile,'w')
    fd.close()
else:
    fd=open(lastFile,'r')
    try:
        fileNumber=int(fd.readline())
    except:
        fileNumber=0
    fd.close()

#print("fileNumber: ",fileNumber)

def extractCoordinates(xmlFile,withFileName=False):
    
    tree = ET.parse(xmlFile)
    root = tree.getroot()
    fileName= root.find('filename').text
    xmin=[]
    ymin=[]
    xmax=[]
    ymax=[]
    for element in root.iter("bndbox"):
        xmin.append(int(element.find("xmin").text))
        ymin.append(int(element.find("ymin").text))
        xmax.append(int(element.find("xmax").text))
        ymax.append(int(element.find("ymax").text))
    
    if not (withFileName):    
        return xmin,xmax,ymin,ymax,len(xmin)
    else:
        return xmin,xmax,ymin,ymax,len(xmin),fileName

def getActionCoordins(xmlFilePath):
    """
    input argument would be xml file path
    output of function is (Action,xmin,xmax,ymin,ymax)
    """
    finalData=[]
    actions=[]
    coordinates=[]
    tree = ET.parse(xmlFilePath)
    root= tree.getroot()
    for element in root.iter("object"):
        for actionNames in element.findall("actions"):

            for names in actionNames.iter():
                if(names.text=='1'):
                    actions.append(names.tag)
                    break
    for element in root.iter("bndbox"):
        subcor=[]
        subcor.append(int(element.find("xmin").text))
        subcor.append(int(element.find("xmax").text))
        subcor.append(int(element.find("ymin").text))
        subcor.append(int(element.find("ymax").text))
        coordinates.append(subcor)
    for action,coordin in zip(actions,coordinates):
        finalData.append((action,coordin))
    
    return finalData 

def updateFile(fileNumber):
    fd=open(lastFile,'w')
    fd.write(str(fileNumber))
    fd.close()


def myZip(*args):
    """
    argument must be list and in this order (xmin,xmax,ymin,ymax)
    it is a generator functin which gives a pair of tuples, first is your top left coordiantes
    and second one is your bottom right coordinates
    """

    xmin=0
    xmax=1
    ymin=2
    ymax=3
    assert (type(args[xmin])==list), "Data type should be list"
    assert (type(args[xmax])==list), "Data type should be list"
    assert (type(args[ymin])==list), "Data type should be list"
    assert (type(args[ymax])==list), "Data type should be list"
    if not(len(args[xmin])==len(args[xmax])==len(args[ymin])==len(args[ymax])):
        print("The length of arguments are not the same\nI will take the minimum length")
    xlenmin=  [len(args[xmin]),len(args[xmax])][len(args[ymin])>len(args[ymax])]
    ylenmin=  [len(args[xmin]),len(args[xmax])][len(args[ymin])>len(args[ymax])]
    totalMinLen= [xlenmin,ylenmin][xlenmin>ylenmin]
    

    for i in range(totalMinLen):
        tl=(args[xmin][i],args[ymin][i])
        br=(args[xmax][i],args[ymax][i])
        yield (tl,br)


xmlList=[]
imgList=[]
xmlFileNumber=[]
imgFileNumber=[]

for xmlFile in os.listdir(AnnFolder):
    if not (xmlFile.endswith("xml")):
        print("invalid file type: %s in %s folder" % (xmlFile,AnnFolder))
    else:
        xmlFileNumber.append(xmlFile.split(".")[0])
        xmlList.append(AnnFolder+"/"+xmlFile)



for imgFile in os.listdir(imageFolder):
    if not ( (len(imgFile.split("."))==2) and (checkExtension(imgFile.split(".")[1]))):
        print("invalid file type: %s in %s folder" % (imgFile,imageFolder))
    else:
        imgFileNumber.append(imgFile.split(".")[0])
        imgList.append(imageFolder+"/"+imgFile)

xmlList.sort()
imgList.sort()


def main():
    global fileAnchor
    fileAnchor=1
    if(len(xmlFileNumber)>len(imgFileNumber)):
        for i in xmlFileNumber:
            if i not in imgFileNumber:
                print("Xml file: %s has no partner" %(i))
        print("Please fix the issue\nProgram is terminated.")
        exit(1)

    elif(len(xmlFileNumber)<len(imgFileNumber)):
        for i in imgFileNumber:
            if i not in xmlFileNumber:
                print("Image file: %s has no partner" %(i))
        print("please fix the issue, program is terminated.")
        exit(1)

    elif(len(xmlList) == len(imgList)):
        fileCounter=1
        print("the quantity of files are the same\n")
        print("%d of %d" %(fileCounter,len(xmlList)))
        #first check the number is the same
        
        for pair in zip(xmlList,imgList):
            
            if (not (fileNumber==0)) and (fileAnchor<fileNumber):
                fileAnchor+=1
                continue
                


            if not (pair[0].split("/")[1].split(".")[0] == pair[1].split("/")[1].split(".")[0]):
                fileAnchor+=1
                print("you have file confiliction at %s %s" % (pair[0],pair[1]))
            
            else:
                fileCounter=fileAnchor
                fileAnchor+=1
                #print('fileAnchor',fileAnchor)
                xmin,xmax,ymin,ymax,bndQty=extractCoordinates(pair[0])
                img=cv2.imread(pair[1])
                for tl,br in myZip(xmin,xmax,ymin,ymax):
                    cv2.rectangle(img,tl,br,(0,255,200),2)
                cv2.namedWindow(pair[1])
                cv2.moveWindow(pair[1],80,80)
                cv2.imshow(pair[1], img)
                while(True):
                    key=cv2.waitKey(1)& 0xff
                    if(key == ord('q')):
                        fileCounter+=1
                        if(fileCounter>len(xmlList)):
                            print("Copy file: %s, %s" %(pair[0],pair[1]))
                            copyfile(pair[0], collected+"/"+pair[0].split("/")[1])
                            copyfile(pair[1], collected+"/"+pair[1].split("/")[1])
                            cv2.destroyAllWindows()
                            break
                        else:
                            print("Copy file: %s, %s" %(pair[0],pair[1]))
                            #copying xml file
                            copyfile(pair[0], collected+"/"+pair[0].split("/")[1])
                            #copying image file
                            copyfile(pair[1], collected+"/"+pair[1].split("/")[1])
                            print("%d of %d" %(fileCounter,len(xmlList)))
                            cv2.destroyAllWindows()
                            break

                    elif(key == ord('l')):
                        fileCounter+=1
                        if(fileCounter>len(xmlList)):
                            cv2.destroyAllWindows()
                            break                 
                        print("%d of %d" %(fileCounter,len(xmlList)))
                        cv2.destroyAllWindows()
                        break

                    elif(key == ord('x')):
                        print("terminating ...")
                        updateFile(fileAnchor-1)
                        #print("fileAnchor:",fileAnchor)
                        cv2.destroyAllWindows()
                        exit(0)


    else:
        
        print("The Quantity of files are not the same")


if __name__ == '__main__': 
    main()