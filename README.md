This program will make your AI and deep learning procedure easier and more accurate.
In this program, you can check whether your dataset has been annotated correctly or not.
the program will open picturs from "Images" folder and also read its annotating coordinates from "Annotations" foldr and put all the rectanglars that you have already setup on pictures.
then you can confrim and copy them (annotation file and image file) into the "Collected" folder by pressing "q", or you can pass it to next picture by pressing 'l'
The program also has the ability to store the location of last file that you have been confirmed/pass by pressing 'x' and will terminate the program.
Thus when you re-open the program, it will start from the last place you were at.

For crop.py file, you can specify the option. Either you want the same file name as picutre for your cropped picture or program takes care of generating name by it self.
can use key "y" followed by switch "-b" to specify that program uses the original name of the picture and create only one cropped file (just first bounding box) per picture. 
"e.g. python3 crop.py -b y"
Or just call crop.py to cropp all the bounding box on the picture, and generate the file name by itself, then saves them in the "Cropped" folder.
e.g. crop.py

In order to use the crop.py file for extracting the actions from the pictures. you need to call -->> crop.py --type  action


Have Fun
