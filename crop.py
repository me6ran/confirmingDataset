from confirmDataset import extractCoordinates,getActionCoordins
import cv2
import os
import argparse

annotationFolder= 'Annotations/'
croppedFolder="cropped/"
imageFolder = "Images/"
outputFileCounter=0
cropFileCounter=0
withFileName=False
isForAction=False
fileList = [xmlFile.split(".")[0] for xmlFile in os.listdir("Annotations/") if(xmlFile.endswith("xml"))]

def makedir(dirName):
    os.mkdir("./cropped/%s" % (dirName))

if not (os.path.isdir(croppedFolder)):
    os.mkdir(croppedFolder)

def cropping():
    global outputFileCounter,withFileName, fileList, cropFileCounter    

    if not (os.path.isdir(croppedFolder)):
        os.mkdir(croppedFolder)

    for fileName in fileList:
        outputFileCounter = 0
        try:
            fullpicName = os.path.join(imageFolder,fileName+".jpg")
            fullxmlName = os.path.join(annotationFolder,fileName+".xml")
            if not (withFileName):
                xmin,xmax,ymin,ymax,bndQty=extractCoordinates(fullxmlName)
                img = cv2.imread(fullpicName)
                for i in range(bndQty):
                    crop_img = img[ymin[i]:ymax[i], xmin[i]:xmax[i]]
                    outputFileName=os.path.join(croppedFolder,"%s_%d.jpg" % (fileName,outputFileCounter))
                    outputFileCounter+=1
                    cv2.imwrite(outputFileName,crop_img)
                    cropFileCounter+=1
            else:
                xmin,xmax,ymin,ymax,bndQty,fileName=extractCoordinates(fullxmlName,withFileName=withFileName)
                img = cv2.imread(fullpicName)
                #for i in range(bndQty):
                crop_img = img[ymin[0]:ymax[0], xmin[0]:xmax[0]]
                outputFileName=os.path.join(croppedFolder,"%s.jpg" % (fileName))
                # outputFileCounter+=1
                cv2.imwrite(outputFileName,crop_img)
        except Exception as e:
            print(e,fileName)
    if not (withFileName):
        print("%d pictures has been cropped from %d files." % (cropFileCounter,len(fileList)) )
    else:
        print("pictures has been cropped from %d files." % (len(fileList)))

def extractActionCoord():
    global fileList
    croppedCounter=0
    _xmin=0
    _xmax=1
    _ymin=2
    _ymax=3
    for fileName in fileList:

        try:
            fullpicName = os.path.join(imageFolder,fileName+".jpg")
            fullxmlName = os.path.join(annotationFolder,fileName+".xml")
            fileDetails = getActionCoordins(fullxmlName)
            for i in range(len(fileDetails)):
                action = fileDetails[i][0]
                coord = fileDetails[i][1]
                croppedCounter+=1
                if not (os.path.isdir("./cropped/%s" % (action))):
                    makedir(action)

                img = cv2.imread(fullpicName)
                #for i in range(bndQty):
                crop_img = img[coord[_ymin]:coord[_ymax], coord[_xmin]:coord[_xmax]]
                outputFileName=os.path.join(os.path.join(croppedFolder,action),"%s_%d.jpg" % (fileName,i))
                #outputFileCounter+=1
                cv2.imwrite(outputFileName,crop_img)

        except Exception as e:
            print(e,fileName)
    listofDir=os.listdir(croppedFolder)
    for directory in listofDir:
        print("%d cropped pictures for action: %s" % (len(os.listdir(os.path.join(croppedFolder,directory))),directory))
    print("In total --> %d pictures has been cropped from %d files." % (croppedCounter,len(fileList)) )

def main():
    if(isForAction):
        extractActionCoord()
    else:
        cropping()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Check whether file name needed or not")
    parser.add_argument("--onebox","-b",default='no',type=str,help="By choosing the switch, program \
    will extarct just one box from xml file and generate the cropped picture file with the same name as the original one")
    parser.add_argument("--type","-t",default='object',type=str,help="the option can be filled with either object/action \
    object is the default value, but if action is specified, program will crop the action and put them in their folders")
    args = parser.parse_args()
    if(args.onebox.upper()[0]=='Y'):
        withFileName=True
    else:
        withFileName=False
    if(args.type.upper()=="ACTION"):
        isForAction=True
    main()